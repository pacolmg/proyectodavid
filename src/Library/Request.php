<?php

namespace App\Library;

class Request 
{
    /** @var array $params */
    private $params;
    /** @var string $params */
    private $method;

    public function __construct(string $method, array $paramsGet = [], array $paramsPost = [])
    {
        $this->method = $method;
        foreach(array_merge($paramsGet, $paramsPost) as $param) {
            $this->params[] = $param;
        }
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getParam(string $name): mixed
    {
        return isset($this->params[$name]) ? $this->params[$name] : null;
    }
}