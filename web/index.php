<?php

require_once realpath('../dbConfig.php');
require_once realpath('../vendor/autoload.php');
require_once realpath('../framework/Container.php');

switch($_GET['path']) {
	case '': 
		echo $controllers['default']->index();
		break;
	case 'publicacion':
		echo $controllers['publicacion']->listado();
		break;
	default:
		echo 'No se encontró la página';
}