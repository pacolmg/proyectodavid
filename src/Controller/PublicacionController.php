<?php

namespace App\Controller;

use App\Library\Request;
use App\Library\ViewHelper;
use App\Repository\PublicacionRepository;

class PublicacionController extends BaseController {
    private $publicacionRepository;

    public function __construct(Request $request, PublicacionRepository $publicacionRepository)
    {
        parent::__construct($request);
        $this->publicacionRepository = $publicacionRepository;
    }

    public function listado()
    {
        $data = $this->publicacionRepository->findAll();
        return ViewHelper::renderPhp('View/PublicacionListadoView.php', ['data' => $data]);
    }
}