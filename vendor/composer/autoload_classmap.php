<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'App\\Controller\\BaseController' => $baseDir . '/src/Controller/BaseController.php',
    'App\\Controller\\PruebaController' => $baseDir . '/src/Controller/PruebaController.php',
    'App\\Library\\Request' => $baseDir . '/src/Library/Request.php',
    'App\\Library\\ViewHelper' => $baseDir . '/src/Library/ViewHelper.php',
);
