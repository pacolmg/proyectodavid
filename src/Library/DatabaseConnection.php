<?php

namespace App\Library;

use \PDO;

class DatabaseConnection
{
    private $connection;

    public function __construct(string $host, string $user, string $password, string $dbName)
    {
        try {
            $conn = new PDO("mysql:host=$host;dbname=$dbName", $user, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection = $conn;
        } catch(\Exception $e) {
            die('No se pudo conectar a la base de datos: ' . $e->getMessage());
        }
    }

    public function getConnection(): PDO
    {
        return $this->connection;
    }

    public function executeQuery(string $query): array
    {
        $result = [];
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        while($row = $stmt->fetch()) {
            $result[] = $row;
        }
        return $result;
    }
}