<?php

namespace App\Controller;

use App\Library\Request;
use App\Library\ViewHelper;
use App\Repository\PublicacionRepository;

class DefaultController extends BaseController {
    public function index()
    {
        return ViewHelper::renderPhp('View/IndexView.php');
    }
}