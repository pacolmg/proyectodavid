<?php

namespace App\Library;

class ViewHelper {
    public static function renderPHP(string $path,array $args = []): string
    {
        ob_start();
        include('../src/' . $path);
        $var=ob_get_contents(); 
        ob_end_clean();
        return $var;
    }
}