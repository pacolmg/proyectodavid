<?php

namespace App\Controller;

use App\Library\Request;

class BaseController
{
    /** @var Request $request */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}