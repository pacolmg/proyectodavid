<?php

namespace App\Model;

class Publicacion
{
    /** @var string $title */
    private $title;
    /** @var string $content */
    private $content;

    public function constructFromArray(array $data)
    {
        $this->title = $data['titulo'];
        $this->content = $data['contenido'];

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }


}