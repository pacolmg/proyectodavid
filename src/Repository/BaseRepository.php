<?php

namespace App\Repository;

use App\Library\DatabaseConnection;

class BaseRepository
{
    private $connection;
    private $table;
    private $model;

    public function __construct(DatabaseConnection $connection, string $table, string $model) {
        $this->connection = $connection;
        $this->table = $table;
        $this->model = $model;
    }

    public function findAll(): array
    {
        $result = [];
        $data = $this->connection->executeQuery("SELECT * FROM {$this->table}");
        foreach($data as $row) {
            $result[] = (new $this->model())->constructFromArray($row);
        }
        
        return $result;
    }
}