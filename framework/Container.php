<?php

use App\Controller\DefaultController;
use App\Controller\PublicacionController;
use App\Library\DatabaseConnection;
use App\Library\Request;
use App\Model\Publicacion;
use App\Repository\PublicacionRepository;

// Crear un objeto Request con los datos de la petición al servidor (método, parámetros get, post...)
$request = new Request($_SERVER['REQUEST_METHOD'], $_GET, $_POST);

// Crear una conexión a la base de datos
$dbConnection = new DatabaseConnection(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

// Cargar las clases Repositorio (Interacciones con la base de datos)
$repositories = [];
$repositories['publicacion'] = new PublicacionRepository($dbConnection, 'publicacion', Publicacion::class);

// Cargar los Controladores
$controllers = [];
$controllers['default'] = new DefaultController($request);
$controllers['publicacion'] = new PublicacionController($request, $repositories['publicacion']);